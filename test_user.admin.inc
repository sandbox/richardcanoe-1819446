<?php

/**
 * @file
 * Admin page callbacks for the test_user module.
 */


/**
 * Form builder for the test user administration form.
 */
function test_user_admin_overview($form, &$form_state) {

  $roles = user_roles('TRUE');
  $current_roles = test_user_get_user_roles();
  $available_roles = array_diff($roles, $current_roles);

  $form['role'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select test users to create'),
    '#options' => $available_roles,
    '#description' => t('Select users to create'),
  );

  $form['users'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select test users to delete'),
    '#options' => $current_roles,
    '#description' => t('Select users to delete'),
  );

  $form['password_confirm'] = array(
    '#type' => 'password_confirm',
    '#input' => TRUE,
    '#process' => array('form_process_password_confirm', 'user_form_process_password_confirm'),
    '#theme_wrappers' => array('form_element'),
    '#description' => t('Create a password for the users you wish to create. This only needs to be used if CREATING users. Leave blank for deleting.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create / Delete'),
  );

  return $form;
}


/**
 * Form validation for the test user administration form.
 */
function test_user_admin_overview_validate($form, &$form_state) {
  $validate = FALSE;
  $roles = $form_state['values']['role'];
  //if (!empty(array_filter($form_state['values']['role']))) {
  foreach ($roles as $key => $value) {
    if ($value != 0) {
      $validate = TRUE;
    }
  }

  if ($validate == TRUE) {

    $pass = trim($form_state['values']['password_confirm']);
    if (drupal_strlen($pass) < 6) {
      form_set_error('password_confirm', t('The password is too short.'));
    }
  }
}

/**
 * Form processing for the test user administration form.
 */
function test_user_admin_overview_submit($form, &$form_state) {

  test_user_create_users($form_state['values']);
  test_user_delete_users($form_state['values']);
}

/**
 * Creates a test user for each authenticated role.
 */
function test_user_create_users($users) {
  foreach ($users['role'] as $key => $value) {
    if ($key == $value) {
      $account = new stdClass();
      $account->is_new = TRUE;
      $edit = array();
      $role = user_role_load($key);
      $name = str_replace(' ', '_', $role->name);
      $user_name = TEST_USER_PREFIX . $name;
      $account->name = $user_name;
      $account->mail = $user_name . '@' . $_SERVER["SERVER_NAME"];
      $account->status = 1;
      $edit['roles'] = array($role->rid => $role->rid);
      $edit['pass'] = $users['password_confirm'];
      $edit['data']['test_user'] = TRUE;

      if (!user_load_by_name($user_name)) {
        if (user_save($account, $edit)) {
          drupal_set_message(check_plain($account->name . ' created'));
        }
      }
      unset($account);
    }
  }
}


/**
 * Deletes test users.
 */
function test_user_delete_users($users) {
  $roles = user_roles('TRUE');
  foreach ($users['users'] as $key => $value) {
    if ($key == $value) {
      $role = user_role_load($value);
      $name = str_replace(' ', '_', $role->name);
      $name = TEST_USER_PREFIX . $name;
      if ($account = user_load_by_name($name)) {
        user_delete($account->uid);
        drupal_set_message(check_plain($name . ' deleted'));
      }
    }
  }
}
