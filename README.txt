As a developer I am often asked to create functionality for a certain user 
role, accessible by this role but not by that. In a world where several 
sites are maintained at once, it can be a time consuming business creating 
users for each role.

This module automatically creates users for each non anonymous role, and 
allows for their easy deletion when testing is complete.
